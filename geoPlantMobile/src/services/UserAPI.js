import mainLink from './mainLink';

const UserAPI={
    login:async(email,password)=>{
        console.warn(email,password);
        return fetch(mainLink+"/users/signIn",{
            method:'post',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password:password
            })
        })
        .then(res=>{
                return res.json();
        })
        .catch(err=>err);
    },
    addUser:(email,password,firstname,lastname,dob,phone,title)=>{
        return fetch(mainLink+"/users",{
            method:'post',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                /*"id": 0,
                "email": "string",
                "password": "string",
                "lastname": "string",
                "firstname": "string",
                "dob": "string",
                "location": 0,
                "title": "string",
                "phone": "string"*/
                email:email,
                password:password,
                firstname:firstname,
                lastname:lastname,
                dob:dob,
                phone:phone,
                title:title
            })
        })
        .then(res=>res)
        .catch(err=>err);
    }
}
export default UserAPI;
