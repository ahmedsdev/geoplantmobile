import Axios from 'axios';
import mainLink from './mainLink';

const FamilyAPI={
    getFamilies:()=>{
        return Axios.get(mainLink+"/plantfamilies")
        .then(res=>res);
    }
}
export default FamilyAPI;