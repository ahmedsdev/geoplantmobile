import Axios from 'axios';
import mainLink from './mainLink';

const PlantAPI={
    addPlant:(name,latName,family,kingdom,plantOrder,geoLat,geoLon,image,about)=>{
        console.log("uslo u plantapi ");
        console.log("ime "+name);
        console.log("porodica "+family);
        console.log("latNaziv "+latName);
        console.log("kraljevstvo "+kingdom);
        console.log("red "+plantOrder);
        //console.log("checked "+checked);
        console.log("lat "+geoLat);
        console.log("lon "+geoLon);
        return fetch(mainLink+"/plants",{
            method:'post',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name:name,
                latName:latName,
                family:family,
                kingdom:kingdom,
                plantOrder:plantOrder,
                geoLat:geoLat,
                geoLon:geoLon,
                image:image,
                about:about
            })
        })
        .then(res=>res)
        .catch(err=>err);
    },
    getBiljke:()=>{
        return Axios.get(mainLink+"/plants")
        .then(res=>res);
    },
    getBijkeById:(id)=>{
        return Axios.get(mainLink+"/plantfamilies/plants/"+id)
        .then(res=>res);
    },
    getBijkeWithId:(id)=>{
        return Axios.get(mainLink+"/plant/"+id)
        .then(res=>res);
    }
}
export default PlantAPI;