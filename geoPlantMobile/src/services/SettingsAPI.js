import Axios from 'axios';
import mainLink from './mainLink';
import moment from "moment";

const SettingsAPI={
    ReportBug:(userID,bug)=>{
        //let date= moment().format('MM DD YYYY, h:mm:ss a');
       let unixDate= moment.utc().valueOf();
        console.log(unixDate);
        console.log(userID);
        console.log(bug);
        let mainUrl = mainLink + "/bugs";
        console.log(mainUrl);
        return fetch(mainUrl,{
            method:'post',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               userid:userID,
               problem:bug,
               timestamp:unixDate,
            })
        })
        .then(res=>res)
        .catch(err=>err);
    },
    
    GetBugsByID:(userID)=>{
        return Axios.get(mainLink+"/bugs/user/"+userID)
         .then(res=>res);
         
     },
}
export default SettingsAPI;