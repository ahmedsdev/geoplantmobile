import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput,ScrollView,TouchableOpacity} from 'react-native';
import UserAPI from '../../services/UserAPI';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';


export default class RegistrationScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            email:"",
            lozinka:"",
            ime:"",
            prezime:"",
            dob:"",
            brojTel:"",
            titula:"",
            lozinka2:"",
        }
    }
    register=()=>{
        if(this.state.email=="" || this.state.lozinka=="" || this.state.ime=="" || this.state.prezime=="" || this.state.dob=="" || this.state.brojTel=="" || this.state.titula==""){
            alert("Neko od polja za unos niste popunili");
        } else{
            if(this.state.lozinka==this.state.lozinka2){
                UserAPI.addUser(this.state.email,this.state.lozinka,this.state.ime,this.state.prezime,this.state.dob,this.state.brojTel,this.state.titula)
                .then((res)=>{
                    console.log(res);
                    if(res.status==200){
                        AsyncStorage.setItem("email",this.state.email);
                        this.props.navigation.naivgate("MainScreen");
                    } else {
                        alert("Korisnik sa unesenim podacima vec postoji");
                    }
                })
            } else {
                alert("Lozinke se ne poklapaju");
            }
        }
    }
    render()
    {
        return(
            <ScrollView contentContainerStyle={styles.container}>
                <ImageBackground source={require('../../assets/slika.jpg')}  style={styles.backgroundImage}>
                   <View style={styles.prijava}><Text style={{fontSize:35,
          color:"#F9F9F9",fontWeight: "bold"}} >Registruj se!</Text>
                   </View>
                   <View style={styles.forma}>
                   <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Ime"}
                           value={this.state.ime}
                           onChangeText={(text)=>{this.setState({ime:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Prezime"}
                           value={this.state.prezime}
                           onChangeText={(text)=>{this.setState({prezime:text})}}
                           ></TextInput>
                       </View> 
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Email"}
                           value={this.state.email}
                           onChangeText={(text)=>{this.setState({email:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Datum rodjenja dd-mm-gggg"}
                           value={this.state.dob}
                           onChangeText={(text)=>{this.setState({dob:text})}}
                           ></TextInput>
                       </View><View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Broj telefona"}
                           value={this.state.brojTel}
                           onChangeText={(text)=>{this.setState({brojTel:text})}}
                           ></TextInput>
                       </View><View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Titula/Zanimanje"}
                           value={this.state.titula}
                           onChangeText={(text)=>{this.setState({titula:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Lozinka"}
                           secureTextEntry
                           value={this.state.lozinka}
                           onChangeText={(text)=>{this.setState({lozinka:text})}}
                           >
                           </TextInput>
                       </View>
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Potvrdi lozinku"}
                           secureTextEntry
                           value={this.state.lozinka2}
                           onChangeText={(text)=>{this.setState({lozinka2:text})}}
                           >
                           </TextInput>
                       </View>
                       <View style={{alignItems:"center",marginTop:35}}>
                           <Text style={{ fontSize:20, color:"#F9F9F9"}}>Već ste registrovani?
                           <Text style={{fontWeight: "bold",fontSize:20, color:"#F9F9F9"}}> Prijavi se.</Text>
                           </Text>
                           
                           
                       </View>
                       <TouchableOpacity onPress={this.register} style={{ marginBottom:40,backgroundColor:"#1E272E", height:50,justifyContent:"center",borderRadius:15,alignItems:"center",marginTop:40}}>
                           

                           <Text style={{ fontWeight: "bold",fontSize:20, color:"#F9F9F9"}} 
                           >REGISTRUJ SE</Text>
                       </TouchableOpacity>

                       
                   </View>

                </ImageBackground>
            </ScrollView>
        );
    }
}

const styles=StyleSheet.create({

    container: {
        
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#F5FCFF',
     flexDirection: 'column',

    },
    backgroundImage:{
        width:"100%",
        height:"100%",
      },
      prijava:{
          
          marginTop:"12%",
          marginLeft:"5%"

      },
      forma:{
        marginLeft:"5%",
        marginRight:"5%",
        marginTop:"20%"

      }
})