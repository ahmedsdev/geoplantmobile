import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput,Image} from 'react-native';


export default class SplashScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
        }
    }
    render()
    {
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../../assets/slika.jpg')}  style={styles.backgroundImage}>
                    <View style={styles.logo}>
                    <Image source={require("../../assets/GeoPlantBiHlogo.png")} style={{width:200,height:200}}></Image>

                    </View>
                    <View>
                        <Text style={{fontSize:35, color:"white", textAlign:"center"}}>Geo Plant BiH</Text>
                    </View>
                  
                  

                </ImageBackground>
            </View>
        );
    }
}

const styles=StyleSheet.create({

    container: {
        flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#F5FCFF',
     flexDirection: 'column',

    },
    backgroundImage:{
        width:"100%",
        height:"100%",
        justifyContent:"center",
        alignItems:"center",
      },
    logo: {

    },
})