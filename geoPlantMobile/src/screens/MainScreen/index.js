import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput,TouchableOpacity} from 'react-native';
import Header from '../../components/Header';
import MapView,{PROVIDER_GOOGLE,Marker} from 'react-native-maps';
import Permissions from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon3 from 'react-native-vector-icons/EvilIcons';


export default class MainScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            isAuthorized:false,
            lat:37.78825,
            long:-122.4324
        }
    }
    componentDidMount()
    {
        Permissions.request('location').then(response => {
            // Returns once the user has chosen to 'allow' or to 'not allow' access
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            console.log("DID MOUNT");
            if(response=='authorized')
            {
                this.setState({isAuthorized:true})
                console.log(response);
                Geolocation.getCurrentPosition((res)=>{
                    console.log(res);
                    this.setState({lat:res.coords.latitude,long:res.coords.longitude})
                })
            }
            
            /*if(response=='authorized')
            {
                console.log("autorizovano");
            }
            else
            {
                console.log("Propalo");
            }*/
          });
    }
    backBtn=()=>{
        console.log("back");
    }
    editBtn=()=>{
        console.log("edit");
    }
    checkPermission=()=>{
        Permissions.request('location').then(response => {
            // Returns once the user has chosen to 'allow' or to 'not allow' access
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            console.log(response);
            
          });
    }

    otvoriMenu = () => {
        this.props.navigation.toggleDrawer();
    }

    render()
    {
        return(
        <View style={styles.container}>
           <View style={styles.UrediProfil}>
                       <View style={{height:"100%",width:"15%",justifyContent:"center",alignItems:"center"}}>
                       <Icon2
                       onPress = {this.otvoriMenu}
                       name="menu"
                       size={40}
                       color="white"
                       ></Icon2>
                       </View>
                       <View style={{height:"100%",width:"70%",justifyContent:"center",alignItems:"center"}}>
                       <Text style={{fontSize:25, 
                        color:"white"}} >GeoPlantBiH</Text>
                       </View>
                       <View style={{height:"100%",width:"15%",justifyContent:"center",alignItems:"center"}}>
                       <Icon3
                     
                       name="bell"
                       size={45}
                       color="white"
                       ></Icon3>
                       </View>

                     </View>
            {this.state.isAuthorized ?
            <View style={styles.contentView}>
                <View style={styles.viewAbs}>
                    <View style={{width:'10%',height:'100%',alignItems:'center',justifyContent:'center',backgroundColor:'#667070', opacity:0.85,borderTopLeftRadius:15,borderBottomLeftRadius:15}}>
                    <Icon name={"magnifying-glass"} size={30} color={"white"}></Icon>
                    </View>
                    <TextInput
                textAlignVertical={'center'}
                style={styles.textInput}
                placeholder={'Pretraži...'}
                placeholderTextColor={'white'}
                ></TextInput>
                </View>
                
                <TouchableOpacity
                style={styles.addBtn}
                >
                    <Icon name={"plus"} size={45} color={"white"}></Icon>

                </TouchableOpacity>
                <MapView
       provider={PROVIDER_GOOGLE} // remove if not using Google Maps
       style={styles.map}
       region={{
         latitude: this.state.lat,
         longitude: this.state.long,
         latitudeDelta: 0.015,
         longitudeDelta: 0.0121,
       }}
     >
       <Marker
       onPress={()=>{console.log("marker pressed")}}
            coordinate={{latitude: this.state.lat,
            longitude: this.state.long}}
            title={"Moja lokacija"}
            description={"Dje smo"}
         />
     </MapView>
            </View>
            :
            <View style={[styles.contentView,{backgroundColor:'green'}]}>

            </View>}
        </View>
        );
    }
}

const styles=StyleSheet.create({

    container: {
       flex:1
    },
    map: {
        flex:1,
        position:'absolute',
        left:0,
        top:0,
        bottom:0,
        right:0
    },
    contentView:{
        backgroundColor:'red',
        flex:1,
    },
    textInput:{
    width:'90%',
    height:'100%',
    backgroundColor:'#667070',
    borderTopRightRadius:15,
    borderBottomRightRadius:15,
    opacity:0.85,
    fontSize:18,
    textAlignVertical:'center'
    },
    viewAbs:{
        position:'absolute',
        top:20,
        left:'5%',
        right:'5%',
        width:'90%',
        height:45,
        flexDirection:'row',
        zIndex:999,
    },
    addBtn:{
        height:60,
        width:60,
        alignItems:'center',
        justifyContent:'center',
        position:'absolute',
        bottom:10,
        right:10,
        zIndex:999,
        backgroundColor:'#4A7761',
        borderRadius:10
    },
    UrediProfil:{
         
        height:70,
        width:"100%",
        backgroundColor:"#31664C",
        flexDirection:"row",
       

    }
})