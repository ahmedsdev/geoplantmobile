import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,ScrollView,TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/AntDesign';
import Icon3 from 'react-native-vector-icons/Feather';
//import {TextInput} from 'react-native-paper';
export default class EditProfileScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            ime:"",
            prezime:"",
            zvanje:"",
            godine:"",
            adresa:"",
            email:"",
            telefon:"",
        }
    }
   
    render()
    {
        return(
            <View style={styles.container}>
                <View style={styles.UrediProfil}>
                       <View style={{height:"100%",width:"15%",justifyContent:"center",alignItems:"center"}}>
                       <Icon
                     
                       name="ios-arrow-back"
                       size={40}
                       color="white"
                       ></Icon>
                       </View>
                       <View style={{height:"100%",width:"70%",justifyContent:"center",alignItems:"center"}}>
                       <Text style={{fontSize:25, 
                        color:"white"}} >Uredi profil</Text>
                       </View>
                          <View style={{height:"100%",width:"15%",justifyContent:"center",alignItems:"center"}}>
                          <Icon2
                      
                       name="check"
                       size={37}
                       color="white"
                       ></Icon2>
                           
                          </View>
                     </View>
                <View style={styles.slikaView}>
                    <View style={{backgroundColor:"white",height:150,width:150,borderRadius:75}}>
                        <View style={styles.kruzic}>
                        <Icon3 
                          name="edit"
                          size={25}
                          color="white"
                     ></Icon3>

                         </View> 
                    </View>
                    
                </View>
                <ScrollView>
                <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%" ,borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Ime:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.ime}
                           onChangeText={(text)=>{this.setState({ime:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%",borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Prezime:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.prezime}
                           onChangeText={(text)=>{this.setState({prezime:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%",borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Zvanje:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.zvanje}
                           onChangeText={(text)=>{this.setState({zvanje:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%",borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Godine:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.godine}
                           onChangeText={(text)=>{this.setState({godine:text})}}
                            ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%",borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Adresa:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.adresa}
                           onChangeText={(text)=>{this.setState({adresa:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%",borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Email:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.email}
                           onChangeText={(text)=>{this.setState({email:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"#282828", height:60,width:"90%",marginLeft:"5%",borderBottomColor:"#595959",borderBottomWidth:1}}>
                           <Text style={{fontSize:16, color:"#315F49",marginTop:11}}>
                               Telefon:
                           </Text>

                           <TextInput style={{ fontSize:17, color:"white",width:"90%",height:40}} placeholder={""}
                           value={this.state.telefon}
                           onChangeText={(text)=>{this.setState({telefon:text})}}
                           ></TextInput>
                       </View>


                </ScrollView>
                 

                           

                            </View>

               
                   
                  

                
            
        );
    }
}

const styles=StyleSheet.create({

    container: {
        flex: 1,
     backgroundColor: '#282828',
     flexDirection: 'column',
    
    
    

    },
      
        slikaView:{
            height:250,
            width:"100%",
            justifyContent:"center",
            alignItems:"center",
            backgroundColor:"#282828",

        },    
   
       
       kruzic:{
           borderRadius:25,
           width:43,
           height:43,
           backgroundColor:"#32664D",
           position:"absolute",
           zIndex:9999,
           bottom:"0%",
           right:"0%",
           alignItems:"center",
           justifyContent:"center",

           
           


       },
   
     

     
      

   
      UrediProfil:{
         
        height:70,
        width:"100%",
        backgroundColor:"#31664C",
        flexDirection:"row",
       

    },
})