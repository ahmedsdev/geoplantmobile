import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,ScrollView,TextInput,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import PlantAPI from '../../services/PlantAPI';



//import {TextInput} from 'react-native-paper';
export default class BiljkaScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
           biljkaDetails:""
        }
    }
   componentDidMount(){
    console.warn(this.props.navigation.getParam("id"));
    let biljkaID=this.props.navigation.getParam("id");
    this.getPlantDetails(biljkaID);
   }
   getPlantDetails=(id)=>{
       PlantAPI.getBijkeWithId(id)
       .then((res)=>{
           this.setState({biljkaDetails:res.data[0]});
           console.warn(res.data);
       });
   }
    render()
    {
        return(
            <View style={styles.container}>
                <View style={styles.SlikaView}>
                <View style={styles.UrediProfil}>
                       <View style={{height:"100%",width:"15%",justifyContent:"center",alignItems:"center"}}>
                       <Icon
                     
                       name="ios-arrow-back"
                       size={40}
                       color="white"
                       ></Icon>
                       </View>
                      
                        
                     </View>
                     <View style={{flexDirection:"row"}}>
                     <View style={{height:70,width:"70%",backgroundColor:"#31664C",marginTop:180,borderBottomColor:"white",borderBottomWidth:1,justifyContent:"center"}}>
                        <Text style={{fontSize:23,color:"white",marginLeft:10}}>
                            {this.state.biljkaDetails.name}
                        </Text>
                        <Text style={{fontSize:17,color:"white",marginLeft:25}}>
                            {this.state.biljkaDetails.latName}
                        </Text>
                        
                     
                     </View>
                     <View style={{height:70,width:"30%",backgroundColor:"#31664C",marginTop:180, borderBottomColor:"white",borderBottomWidth:1,justifyContent:"center",alignItems:"center"}}>
                          <View style={{height:20,width:70,backgroundColor:"white",borderRadius:15,justifyContent:"center",opacity:0.7}}>
                             <TouchableOpacity style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                 
                              <Icon2
                              name="share"
                              size={10}

                              ></Icon2>
                              <Text style={{fontSize:13,marginLeft:5,color:"black"}}>
                                  Podijeli
                              </Text>
                              
                             </TouchableOpacity>


                          </View>
                     </View>
                     
                     </View>
                </View>
               
                 <ScrollView contentContainerStyle={{alignItems:"center"}}>
                     <View style={{width:"90%",height:140,backgroundColor:"#282828",alignItems:"center"}}>
                         <Text style={{fontSize:20,color:"white"}}>
                             Opis biljke
                         </Text>
                         <View style={{width:"90%",height:100,backgroundColor:"#282828"}}>
                         <ScrollView nestedScrollEnabled={true}>
                             <Text style={{fontSize:15,color:"white"}} >
                                {this.state.biljkaDetails.about}
                             </Text>
                         </ScrollView>
                         </View>
                     </View>
                     <View style={styles.podaciView}>
                         <Text style={{color:"white"}}>
                         {this.state.biljkaDetails.kingdom}
                         </Text>
                         

                     </View>
                     <View style={styles.podaciView}>
                         <Text style={{color:"white"}}>
                         {this.state.biljkaDetails.plantOrder}
                         </Text>
                         

                     </View>
                     <View style={styles.podaciView}>
                         <Text style={{color:"white"}}>
                         {this.state.biljkaDetails.family}
                         </Text>
                         

                     </View>
                     <View style={styles.podaciView}>
                         <Text style={{color:"white"}}>
                           Autor
                         </Text>
                         

                     </View>
                     <View style={{width:"90%",height:200,backgroundColor:"green",marginTop:20,marginBottom:20}}>
                          
                         

                     </View>

                 </ScrollView>

                           

                           </View>

               
                   
                  

                
            
        );
    }
}

const styles=StyleSheet.create({

    container: {
        flex: 1,
     backgroundColor: '#282828',
     flexDirection: 'column',
    
    
    

    },
    SlikaView:{
     width:"100%",
     height:320,
     backgroundColor:"white"
    },
   
     

     
      

   
      UrediProfil:{
         
        height:70,
        width:"100%",
        backgroundColor:"red",
        flexDirection:"row",
        opacity:0.4

    },
    podaciView:{
        width:"90%",
        height:45,
        backgroundColor:"#282828",
       alignItems:"center",
        borderBottomColor:"white",
        borderBottomWidth:1,
        flexDirection:"row",
        
    
    },
})