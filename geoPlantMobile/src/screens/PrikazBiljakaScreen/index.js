import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput,ScrollView,FlatList,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import Porodica from '../../components/Porodica';
import PlantAPI from '../../services/PlantAPI';
import Header from '../../components/Header';
import Biljka from '../../components/Biljka';


export default class PrikazBiljakaScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            pretraga:"",
            nizBiljaka:[]
        }
    }
    componentDidMount(){
       let familyId = this.props.navigation.getParam('id');
        /*PlantAPI.getBiljke()
        .then((res)=>{
            console.log(res);
            console.log("islo u res");
            this.setState({
             nizBiljaka:res.data
            });
        }) */
        PlantAPI.getBijkeById(familyId)
        .then((res)=>{
            console.log(res);
            this.setState({
                nizBiljaka:res.data
            })
        })
    }
    mapirajBiljke=()=>{
        console.log(this.state.nizBiljaka);
        console.log("ovo je niz biljaka");
        return this.state.nizBiljaka.map((element)=>{
            return <TouchableOpacity  onPress={()=>this.viseInfo(element.id)} style={{width:'100%',height:130,marginBottom:10}} key={element.id}>
            <Biljka biljka={element}/>
            </TouchableOpacity>;
        })
    }
    viseInfo=(id)=>{
        this.props.navigation.navigate("BiljkaScreen",{id:id});

    }
    render()
    {
        return(
            <View style={styles.container}>
                
                <Header naslov = "Prikaz biljaka"/>
                           
                            <View style={{height:40, alignItems:"center", justifyContent:"center",flexDirection:"row",marginTop:20}}>
                                <View style={{width:"10%",backgroundColor:'#7B7B7B',borderBottomLeftRadius:25,borderTopLeftRadius:25 ,height:"100%",justifyContent:"center",alignItems:"center"}}>
                                 <Icon2
                               name="search"
                               size={18}
                               color="white" 

                                                           
                               ></Icon2>
                                </View>
                            
                               <TextInput style={{backgroundColor:'#7B7B7B',borderBottomRightRadius:25 ,borderTopRightRadius:25 , fontSize:13,color:"white",height:40,width:"80%" }} placeholder={"Pretraži"} placeholderTextColor="white" 
                           value={this.state.pretraga}
                           onChangeText={(text)=>{this.setState({pretraga:text})}}
                           >
                               
                           </TextInput>
                            </View>
                           
                       
                      
                       <View style={styles.skupSlika}>

                       <ScrollView contentContainerStyle={{width:"100%",flexDirection:"row",justifyContent:'space-evenly', flexWrap: 'wrap'}}>
                           
                           {this.state.nizBiljaka.length>0 && this.mapirajBiljke()}
                       </ScrollView> 
                       </View>
              
              
            </View>

        );
    }
}

const styles=StyleSheet.create({

    container: {
        flex: 1,
     backgroundColor: '#282828',
     flexDirection: 'column',

    },
    
      prikazPorodica:{
         
          height:"10%",
          width:"100%",
          backgroundColor:"#31664C",
          alignItems:"center",
          justifyContent:"center",
         marginBottom:"5%",
          

      },

    slike:{
        width:"30%",
        height:"30%",
        backgroundColor:"red",
        marginTop:"3%",
        marginLeft:"3%",
    },

    skupSlika:{
    height:" 75%",
    width:"90%",
     backgroundColor:"#282828",
     marginTop:"5%",
     marginLeft:"5%",
     marginRight:'5%'
    },
})