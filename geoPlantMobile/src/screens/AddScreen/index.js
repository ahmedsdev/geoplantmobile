import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput,Image,ScrollView,CheckBox,Dimensions,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { RadioButton } from 'react-native-paper';
import PlantAPI from '../../services/PlantAPI';
import Header from "../../components/Header";

const height=Dimensions.get("screen").height;

export default class AddScreen extends Component{
    constructor(props)
    {  
  
        super(props);
        this.state={
            opis:"",
            lozinka:"",
            ime:"",
            porodica:"",
            latNaziv:"",
            kraljevstvo:"",
            red:"",
            checked: 'otrovna',
            lat:"",
            lon:""
        }
    }
    backBtn=()=>{
        console.log("back");
    }
    unesiBiljku=()=>{
        const {opis,lozinka,ime,porodica,latNaziv,kraljevstvo,red,checked,lat,lon}=this.state;
        console.log("opis "+opis);
        console.log("lozinka "+lozinka);
        console.log("ime "+ime);
        console.log("porodica "+porodica);
        console.log("latNaziv "+latNaziv);
        console.log("kraljevstvo "+kraljevstvo);
        console.log("red "+red);
        console.log("checked "+checked);
        console.log("lat "+lat);
        console.log("lon "+lon);
        PlantAPI.addPlant(ime,latNaziv,porodica,kraljevstvo,red,lat,lon,"",opis)
        .then((res)=>{
          console.log(res);
        })
        .catch(err=>{
          console.log(err);
        })
    }
    render()
    {
        const { checked } = this.state;
        return(
            <ScrollView contentContainerStyle={styles.container}>
              <Header naslov = {"Dodaj biljku"}/> 
              <View style={styles.container2}>
              <View style={styles.slika}>
              <Image style={{height:"100%",width:"100%", borderRadius:30}}
                source={require("../../assets/slika.jpg")}></Image>
 

              </View>
              <View style={styles.forma}>
                 <TextInput style={{justifyContent:"flex-end",height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={"Ime:"} placeholderTextColor={"white"}
                           value={this.state.ime}
                           onChangeText={(text)=>{this.setState({ime:text})}}
                           ></TextInput>
                           <TextInput style={{height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={"Latinski naziv:"} placeholderTextColor={"white"}
                           value={this.state.latNaziv}
                           onChangeText={(text)=>{this.setState({latNaziv:text})}}
                           ></TextInput>
                           <TextInput style={{justifyContent:"flex-end",height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={"Porodica:"} placeholderTextColor={"white"}
                           value={this.state.porodica}
                           onChangeText={(text)=>{this.setState({porodica:text})}}
                           ></TextInput>
                           <TextInput style={{justifyContent:"flex-end",height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={"Red:"} placeholderTextColor={"white"}
                           value={this.state.red}
                           onChangeText={(text)=>{this.setState({red:text})}}
                           ></TextInput>
                           <TextInput style={{justifyContent:"flex-end",height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={"Kraljevstvo:"} placeholderTextColor={"white"}
                           value={this.state.kraljevstvo}
                           onChangeText={(text)=>{this.setState({kraljevstvo:text})}}
                           ></TextInput>
              </View>
              <View style={{marginTop:30,flexDirection:"row",justifyContent:"center",alignItems:"center",height:80}}>
                 <View style={{width:"20%"}}> 
                 <Text style={{fontSize:19,color:"#F9F9F9"}}>Opis</Text>
                 </View>
                  <View style={{width:"80%",borderRadius:30,borderColor:"white",borderWidth:1,height:80}}>
                      <TextInput style={{color:"white",fontSize:18,marginTop:-6,marginLeft:3,marginRight:3}}  value={this.state.opis}
                           onChangeText={(text)=>{this.setState({opis:text})}} 
                           multiline={true}></TextInput>
                  </View>
              </View>
              <RadioButton.Group
        onValueChange={value => {this.setState({ checked:value });console.log("VALUE CHECKEDA ");console.log(value);}}
        value={this.state.checked}
      >
              
              <View style={{flexDirection:"row",marginTop:30,width:"100%",justifyContent:"space-between",alignItems:"center"}}>
              <Text style={{fontSize:19,color:"#F9F9F9"}}>Otrovna</Text>   
        <RadioButton
           color={"#32664D"}
          
          value="otrovna"
        uncheckedColor={"white"}
          
        /></View>
       <View style={{flexDirection:"row",width:"100%",justifyContent:"space-between",alignItems:"center"}}>
            <Text style={{fontSize:19,color:"#F9F9F9"}}>Neutralna</Text>
        <RadioButton
        color={"#32664D"}
        
        uncheckedColor={"white"}
          value="neutralna"
          
        /></View>
        <View style={{flexDirection:"row",width:"100%",justifyContent:"space-between",alignItems:"center"}}>
            <Text style={{fontSize:19,color:"#F9F9F9"}}>Ljekovita</Text>
        <RadioButton
        color={"#32664D"}
        
        uncheckedColor={"white"}
          value="ljekovita"
          
        /></View></RadioButton.Group>
             <View style={{justifyContent:"center",alignItems:"center",marginBottom:15}}><Text style={{marginTop:15,fontSize:22,color:"#32664D",fontWeight:"bold"}}>Lokacija</Text></View>
             <View style={{flexDirection:"row",height:200,marginTop:20}}>
                 <View style={{width:"30%",justifyContent:"center",alignItems:"center"}}>
                     <Text style={{fontSize:19,color:"white",fontStyle:"italic"}}>Lat</Text>
                     <TextInput style={{justifyContent:"flex-end",height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={""} 
                           value={this.state.lat}
                           onChangeText={(text)=>{this.setState({lat:text})}}
                           ></TextInput>
                     <Text style={{fontSize:19,color:"white",fontStyle:"italic",marginTop:15}}>Lon</Text>
                     <TextInput style={{justifyContent:"flex-end",height:"20%",width:"100%", fontSize:19, color:"#F9F9F9",borderBottomColor:"white",borderBottomWidth:1}} placeholder={""} 
                           value={this.state.lon}
                           onChangeText={(text)=>{this.setState({lon:text})}}
                           ></TextInput>
                 </View>
                 <View style={{borderRadius:30,width:"64%",marginLeft:20,backgroundColor:"white",height:200}}></View>
                 
                 
                 </View> 
                 <View style={{marginTop:35,heigth:30,alignItems:"flex-end",justifyContent:"flex-end",marginEnd:20}}>
                    <TouchableOpacity onPress={this.unesiBiljku}><Text style={{fontSize:30,color:"#32664D",fontWeight:"bold",letterSpacing:3}}>UNESI</Text></TouchableOpacity>
                 </View>
            
                
              </View>
  
            </ScrollView>
        );
    }
}

const styles=StyleSheet.create({

    container: {
     minHeight:height+550,
     alignItems: 'center',
     backgroundColor: '#1E272E',
     paddingBottom:5,
     justifyContent:"space-evenly"
     
     

    },
    container2: {
      flex:1,  
     width:"85%",

     
     
     backgroundColor: '#1E272E',
     

    },
    unosView:{
        width:"95%",
        height:60,
        backgroundColor:"#32664D",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    
      },
      slika:{
          
          marginTop:20,
          width:"100%",
          borderRadius:20,
          height:300,
          alignItems:"center",
        justifyContent:"center"

      },
      forma:{
       
        marginTop:5,
        width:"100%",
        height:300

        }
        
})