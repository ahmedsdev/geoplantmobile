import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,Switch,TouchableOpacity,Image} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/AntDesign';






export default class StatisticScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={}
        
    }
    render()
    {
        return(
        <View style={styles.container}>
            <View style={styles.statistika}>
            
                <Icon1
            style={{position:"absolute", left:"5%", top:"40%"} }
                            size={30} 
                            color={"white"} 
                            name={"arrowleft"} 
                            onPress={this.backBtn} >
            
            </Icon1>
            <Text style={{fontSize:25,color:"white",textAlign:'center', marginTop:"5%"}}>Statistika</Text>

            </View>
            <View style={styles.elementi}>
                <TouchableOpacity onPress={()=>{console.log("UKUPNO BILJAKA")}} style={[styles.podelement,{backgroundColor:"#9CBD6A", borderRadius:30}]}>

                    <Text style= {{fontSize:35, color:"white", textAlign:"center", fontWeight:"bold"}}>256  
                    <Image source={require("../../assets/biljka.png")} style={{width:32,height:32}}></Image></Text>

                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>UKUPNO</Text>
                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>BILJAKA</Text>

                 

                </TouchableOpacity>
              
                <TouchableOpacity onPress={()=>{console.log("KORISNIKA")}} style={[styles.podelement,{backgroundColor:"#7BC78D", borderRadius:30}]}>
                    <Text style= {{fontSize:35, color:"white", textAlign:"center", fontWeight:"bold"}}>332 
                    <Image source={require("../../assets/korisnici.png")} style={{width:32,height:32}}></Image></Text>

                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>KORISNIKA</Text>
                   
                </TouchableOpacity>
                
            </View>
            
            <View style={styles.elementi}>
            <TouchableOpacity onPress={()=>{console.log("LOKACIJA")}} style={[styles.podelement,{backgroundColor:"#45ADA8", borderRadius:30}]}>

            
                    <Text style= {{fontSize:35, color:"white", textAlign:"center", fontWeight:"bold"}}>700  
                    <Image source={require("../../assets/lokacija.png")} style={{width:32,height:32}}></Image></Text>

                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>LOKACIJA</Text>
                  
</TouchableOpacity>
<TouchableOpacity onPress={()=>{console.log("UKUPNO PORODICA")}} style={[styles.podelement,{backgroundColor:"#547980", borderRadius:30}]}>
                    <Text style= {{fontSize:35, color:"white", textAlign:"center", fontWeight:"bold"}}>526 
                    <Image source={require("../../assets/menu.png")} style={{width:32,height:32}}></Image></Text>

                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>UKUPNO</Text>
                  
                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>PORODICA</Text>

                 
                </TouchableOpacity>
            </View>

            <View style={styles.elementi}>
            <TouchableOpacity onPress={()=>{console.log("RIJEŠENIH BILJAKA")}} style={[styles.podelement,{backgroundColor:"#3498DB", borderRadius:30}]}>

                    <Text style= {{fontSize:35, color:"white", textAlign:"center", fontWeight:"bold"}}>256 
                    <Image source={require("../../assets/correct.png")} style={{width:32,height:32}}></Image></Text>

                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>RIJEŠENIH</Text>
                  
                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>BILJAKA</Text>

                 
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{console.log("NERIJEŠENIH BILJAKA")}} style={[styles.podelement,{backgroundColor:"#594F4F", borderRadius:30}]}>
                    <Text style= {{fontSize:35, color:"white", textAlign:"center", fontWeight:"bold"}}>256 
                    <Image source={require("../../assets/iks.png")} style={{width:32,height:32}}></Image></Text>

                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>NERIJEŠENIH</Text>
                  
                    <Text style= {{fontSize:25, color:"white", textAlign:"center"}}>BILJAKA</Text>

                 
                </TouchableOpacity>
            </View>

        </View>
        )}
        
        
}

           
           
           
const styles=StyleSheet.create({
    jedanRed:{
        flexDirection:'row',
        height:'100%',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: 15,
        paddingVertical: 5
    },
     
    container: {
        flex: 1,
        backgroundColor: '#282828',
        flexDirection: 'column',
        
            },
            statistika:{
                width:"100%",
                height:"10%",
                marginTop:"5%",
                backgroundColor:"#32664D",
            },
            elementi: {
                width:"100%",
                height:"20%",
        
                flexDirection:"row",
                justifyContent:"space-around",
                marginTop:30
                

            },
                podelement : {
                    width:"45%",
                    height:"100%",
                

                },
            
        })
