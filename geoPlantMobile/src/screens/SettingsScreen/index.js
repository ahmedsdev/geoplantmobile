import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,Switch,TouchableOpacity,TextInput} from 'react-native';
import Modal from "react-native-modal";




export default class SettingsScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            switchCamera: false,
            switchLocation: false,
            switchGallery: false,
            switchAutor: false,
            modalVisible: false,
            bugReport:"",


        }
    }
    posaljiBugReport=() =>{
        console.log(this.state.bugReport);
    }

    render()
    {
        return(
            <View style={styles.container}>
                <Modal isVisible={this.state.modalVisible}>
                    <View style={styles.posalji}></View>
           <View style={{ width:'100%' ,height:350 ,backgroundColor:"#32664D",borderRadius:30,alignItems:"center",justifyContent:"center" }}>
            <Text style={{color:"white", fontSize:16, textAlign:"left",marginHorizontal:"10%"}}>Imate problem s našom aplikacijom? Ostavite povratne informacije ili podijelite svoje ideje s nama!</Text>
            
            <View style={styles.napisinesto}></View>
            {/*/<View style={{ width:"80%", height:"55%",backgroundColor:"black",borderRadius:30,marginHorizontal:"10%", marginTop:"7%"}}>
                <Text style={{color:"white", fontSize:15,textAlign:"left",marginLeft:"6%"}}>Napiši nešto...</Text>

        </View>*/}
        <TextInput style={{backgroundColor:"black",borderRadius:30,marginHorizontal:"5%",width:"90%", height:200,marginTop:"7%",color:"white",textAlignVertical:"top"}}
        value={this.state.bugReport}
        onChangeText={(text)=>{this.setState({bugReport:text})}}
        multiline={true}
        placeholder={"Napiši nešto..."}
        placeholderTextColor={"white"}
        
        
        >
        
        </TextInput>
        </View>

        <View style={{height:20,backgroundColor:"black", width:"100%"}}></View>
        <View style={{width:'100%',alignItems:'center',justifyContent:'center'}}>
        <View style={{ width:'40%' ,height:50 ,backgroundColor:'#32664D',borderRadius:30 ,marginTop:20,alignItems:'center',justifyContent:'center',width:'40%'}}>
        <TouchableOpacity onPress={this.posaljiBugReport} style={{alignContent:"center",alignItems:"center"}}> 
                                <Text style={{fontSize:20,color:"white"}}>Pošalji</Text>

                                </TouchableOpacity>
                                </View>
        </View>
        
        <View style={styles.dugme}></View>
      

          
        </Modal>
                
                    <View style={styles.podjela1}>
                        <View style={styles.podjela2}>
                            <View style={styles.podjela3}>
                                <Text style={{fontSize:25,color:"white", textAlign:"center", fontWeight:"bold"}} >Postavke</Text>

                                
                            <View style={styles.podjelapodrska}></View>
                            
                                <Text style={{fontSize:19,color:"white", fontWeight:"bold",marginLeft:5,}}>PODRŠKA</Text>
                                <TouchableOpacity onPress={()=>{console.log("Prijavi problem")}}style={{backgroundColor:'red',position:'absolute',zIndex:999,top:'170%'}}> 
                                <Text style={{fontSize:13,color:"white"}}>Prijavi problem</Text>

                                </TouchableOpacity>
                                
                                


                            </View>
                        </View>
                        <View style={styles.podjela55}>
                            <Text style={{fontSize:19, color:"white", fontWeight:"bold",marginLeft:5}}>SIGURNOST</Text>
                          <View style={styles.jedanRed}> 
                            <Text style={{fontSize:13,color:"white"}}>Kamera</Text>
                            <Switch 
                                value={this.state.switchCamera}
                                onValueChange={(value) => this.setState({switchCamera: value})}
                            >

                            </Switch>
                        </View>
    
            <View style={styles.podjelasigurnost}>
            <View style={styles.jedanRed}> 
                            <Text style={{fontSize:13,color:"white"}}>Lokacija</Text>
                            <Switch 
                                value={this.state.switchLocation}
                                onValueChange={(value) => this.setState({switchLocation: value})}
                            >

                            </Switch>
                        </View>


            
                </View>
                <View style={styles.podjelasigurnost}>
                <View style={styles.jedanRed}> 
                            <Text style={{fontSize:13,color:"white"}}>Galerija</Text>
                            <Switch 
                                value={this.state.switchGallery}
                                onValueChange={(value) => this.setState({switchGallery: value})}
                            >

                            </Switch>
                        </View>


            
</View>
<View style={styles.podjelasigurnost}>
<View style={styles.jedanRed}> 
                            <Text style={{fontSize:13,color:"white"}}>Automatska pretraga</Text>
                            <Switch 
                                value={this.state.switchAutor}
                                onValueChange={(value) => this.setState({switchAutor: value})}
                            >

                            </Switch>
                        </View>


            
                </View>
            <View style={styles.zadnjapodjela}>
                
            <TouchableOpacity onPress={()=>{this.setState({modalVisible:!this.state.modalVisible});}}style={{backgroundColor:'red'}}>
                
            <Text style={{fontSize:13,color:"white", marginBottom:5,marginTop:10,marginLeft:5}}>Odjavi se</Text>
            </TouchableOpacity>

                </View>    


            </View>
            </View>
            </View>
            
        );
    }
}

const styles=StyleSheet.create({
    jedanRed:{
        flexDirection:'row',
        width:'100%',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: 15,
        paddingVertical: 5
    },

    container: {
        flex: 1,
     backgroundColor: '#282828',
     flexDirection: 'column',

    },
   
        podjela1:{
            width:"100%",
            height:"45%",
            borderColor:"white",
            borderWidth:0.4

      },
            podjela2:{
                width:"100%",
                height:"45%",
                borderColor:"white",
                borderWidth:0.4,
            },
            podjela55:{
                width:"100%",
                height:"55%",
                


            },
                podjelasigurnost:{
                    width:"100%",
                    height:"20%",
                    borderColor:"white",
                    borderTopWidth:0.4,
                    
                    
                },
               
            
                  
    
                podjela3:{
                    width:"100%",
                    height:"43%",
                    backgroundColor:"#32664D",
                },
                podjelapodrska:{
                    width:"100%",
                    height:"57%",
                
                },
    zadnjapodjela:{
        width:"100%",
        height:"55%",


    },
    posalji:{
        

            },
        napisinesto:{
           

        },
        dugme:{

        },
})