import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,Switch,TouchableOpacity,TextInput} from 'react-native';
import Modal from "react-native-modal";
import Header from "../../components/Header";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SettingsAPI from  "../../services/SettingsAPI";
import AsyncStorage from '@react-native-community/async-storage';



export default class SettingsScreen2 extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            switchCamera: false,
            switchLocation: false,
            switchGallery: false,
            switchAutomaticSearch: false,
            modalVisible: false,
            bugReport:"",


        }
    }
    odjava=async()=>{
        await AsyncStorage.removeItem("email");
        this.props.navigation.navigate("LoginScreen");
    }

    posaljiBugReport=() =>{
        console.log(this.state.bugReport);
          SettingsAPI.ReportBug(1,this.state.bugReport)
          .then((res)=>{
              console.log(res);
              this.setState({bugReport:"",modalVisible:false});
          })
       
       

        }

    render()
    {
        return(
            <View style={styles.container}>
                <Modal isVisible={this.state.modalVisible} onBackdropPress={()=>{this.setState({modalVisible:false})}} >
                    <View style={styles.posalji}></View>
           <View style={{ width:'100%' ,height:350 ,backgroundColor:"#32664D",borderRadius:30,alignItems:"center",justifyContent:"center" }}>
            <Text style={{color:"white", fontSize:16, textAlign:"left",marginHorizontal:"10%"}}>Imate problem s našom aplikacijom? Ostavite povratne informacije ili podijelite svoje ideje s nama!</Text>
            
            <View style={styles.napisinesto}></View>
            {/*/<View style={{ width:"80%", height:"55%",backgroundColor:"black",borderRadius:30,marginHorizontal:"10%", marginTop:"7%"}}>
                <Text style={{color:"white", fontSize:15,textAlign:"left",marginLeft:"6%"}}>Napiši nešto...</Text>

        </View>*/}
        <TextInput style={{backgroundColor:"black",borderRadius:30,marginHorizontal:"5%",width:"90%", height:200,marginTop:"7%",color:"white",textAlignVertical:"top"}}
        value={this.state.bugReport}
        onChangeText={(text)=>{this.setState({bugReport:text})}}
        multiline={true}
        placeholder={"Napiši nešto..."}
        placeholderTextColor={"white"}
        
        
        >
        
        </TextInput>
        </View>

        <View style={{height:20,backgroundColor:"black", width:"100%"}}></View>
        <View style={{width:'100%',alignItems:'center',justifyContent:'center'}}>
        <View style={{ width:'40%' ,height:50 ,backgroundColor:'#32664D',borderRadius:30 ,marginTop:20,alignItems:'center',justifyContent:'center',width:'40%'}}>
        <TouchableOpacity onPress={this.posaljiBugReport} style={{alignContent:"center",alignItems:"center"}}> 
                                <Text style={{fontSize:20,color:"white"}}>Pošalji</Text>

                                </TouchableOpacity>
                                </View>
        </View>
        
        <View style={styles.dugme}></View>
      

          
        </Modal>
         <Header naslov="Postavke"/>
             <View style={styles.gornjePola}>
                 <View style={{width:"100%",height:60}}>
                     <Text style={{fontSize:21,fontWeight:"bold",color:"white"}}>
                         PODRŠKA
                     </Text>
                     </View>
                    
                     <TouchableOpacity  onPress={()=>{this.setState({modalVisible:!this.state.modalVisible})}}style={{position:'absolute',zIndex:999,top:33}}> 
                                <Text style={{fontSize:15,color:"white"}}>Prijavi problem</Text>

                      </TouchableOpacity>
                    
                     
                 <View style={[styles.sigurnostView,{borderTopColor:"white",borderTopWidth:1}]}>
                 <Text style={{fontSize:19, color:"white", fontWeight:"bold",marginLeft:5}}>SIGURNOST</Text>
                  <View style={[styles.sigurnostOpcije,{borderBottomColor:"gray",borderBottomWidth:0.5}]}>
                     <Text style={{fontSize:15,marginLeft:10,color:"white"}}>
                         Kamera
                     </Text>
                     <Switch 
                                value={this.state.switchCamera}
                                onValueChange={(value) => this.setState({switchCamera: value})}
                            >

                     </Switch>
                            
                          
                      
                 </View>
                 <View style={[styles.sigurnostOpcije,{borderBottomColor:"gray",borderBottomWidth:0.5}]}>
                     <Text style={{fontSize:15,marginLeft:10,color:"white"}}>
                         Lokacija
                     </Text>
                     <Switch 
                                value={this.state.switchLocation}
                                onValueChange={(value) => this.setState({switchLocation: value})}
                            >

                     </Switch>  
                  </View>

                  <View style={[styles.sigurnostOpcije,{borderBottomColor:"gray",borderBottomWidth:0.5}]}>
                     <Text style={{fontSize:15,marginLeft:10,color:"white"}}>
                         Galerija
                     </Text>
                     <Switch 
                                value={this.state.switchGallery}
                                onValueChange={(value) => this.setState({switchGallery: value})}
                            >

                     </Switch>  
                      
                </View>
                <View style={[styles.sigurnostOpcije,{borderBottomColor:"white",borderBottomWidth:1}]}>
                     <Text style={{fontSize:15,marginLeft:10,color:"white"}}>
                        Automatska pretraga
                     </Text>
                     <Switch 
                                value={this.state.switchAutomaticSearch}
                                onValueChange={(value) => this.setState({switchAutomaticSearch: value})}
                            >

                     </Switch>
                            
                          
                      
                </View>
                    
                   </View>
               <Text style={{fontSize:15,color:"white"}}>
                   Prijavljeni ste kao:" "
               </Text>
              
               <TouchableOpacity style={{flexDirection:"row"}} onPress={this.odjava}>
               <Icon
                    name="logout"
                    size={17}
                    color="white"
                   >  </Icon>
                
                <Text style={{fontSize:15,color:"white"}}>Odjavi se</Text>
                </TouchableOpacity>
              
           

             </View>
                
                   
            </View>
           
            
        );
    }
}

const styles=StyleSheet.create({
    jedanRed:{
        flexDirection:'row',
        width:'100%',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: 15,
        paddingVertical: 5
    },

    container: {
        flex: 1,
     backgroundColor: '#282828',
     flexDirection: 'column',
     backgroundColor:"#282828"

    },
    gornjePola:{
        width:"100%",
        height:"47%",
        backgroundColor:"#282828"

    },
    sigurnostView:{
        width:"100%",
        height:170,
        backgroundColor:"#282828",
        

    },
    sigurnostOpcije:{
        width:"100%",
        height:35,
        backgroundColor:"#282828",
        color:"white",
        alignItems:"center",
        flexDirection:"row",
        justifyContent:"space-between"
          
        


    },


    
    posalji:{
        

            },
        napisinesto:{
           

        },
        dugme:{

        },
})