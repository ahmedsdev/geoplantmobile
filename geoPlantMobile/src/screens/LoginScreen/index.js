import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput,TouchableOpacity} from 'react-native';
import UserAPI from '../../services/UserAPI';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';

export default class LoginScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            email:"",
            lozinka:"",
        }
    }
    componentDidMount()
    {
          this.jelUlogovan();
          SplashScreen.hide();
    }
    jelUlogovan=async()=>{
        let email=await AsyncStorage.getItem("email");
        if(email && email.length>0){
            this.props.navigation.navigate("MainScreen");
        }
    }
    login=async()=>{
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(this.state.email.length>0){
            if(re.test(this.state.email.toLowerCase()))
            {
                if(this.state.lozinka.length>0){
                    console.log("OVO SU PODACI");
                    console.log(this.state.email,this.state.lozinka);
                    UserAPI.login(this.state.email,this.state.lozinka)
                    .then((res)=>{
                        console.log(res);
                        console.log(res);
                        if(res.message && res.message == 'User not found!'){
                            alert("KORISNIK SA UNESENIM PODACIMA NE POSTOJI!");
                            return;
                        } else if(res.message && res.message == 'Password is incorrect!'){
                            alert("POGREŠNA LOZINKA!");
                        } else if(!res.message){
                            AsyncStorage.setItem("email",this.state.email);
                            this.props.navigation.navigate("MainScreen");
                        }

                    })
                    .catch(err=>{
                        console.log("Uslo u error");
                        console.log(err);
                    })
                }
                else{
                    alert("LOZINKA NIJE UNESENA!");
                }
            }
            else{
                alert("MORA BITI U E-MAIL FORMATU!");
            }
        }
        else{
            alert("E-MAIL NIJE TAČAN!");
            
        }
    }
    render()
    {
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../../assets/slika.jpg')}  style={styles.backgroundImage}>
                   <View style={styles.prijava}><Text style={{fontSize:35,
          color:"#F9F9F9",fontWeight: "bold"}} >Prijavi se!</Text>
                   </View>
                   <View style={styles.forma}>
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15}}>
                           

                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Email"}
                           value={this.state.email}
                           onChangeText={(text)=>{this.setState({email:text})}}
                           ></TextInput>
                       </View>
                       <View style={{ backgroundColor:"white", height:50,justifyContent:"center",borderRadius:15,marginTop:35}}>
                           <TextInput style={{ fontSize:20, color:"black",marginLeft:"3%"}} placeholder={"Lozinka"}
                           secureTextEntry
                           value={this.state.lozinka}
                           onChangeText={(text)=>{this.setState({lozinka:text})}}
                           >
                           </TextInput>
                       </View>
                       <View style={{alignItems:"center",marginTop:35}}>
                           <Text style={{ fontSize:20, color:"#F9F9F9"}}>Nemate profil?
                             <Text style={{fontWeight: "bold",fontSize:20, color:"#F9F9F9"}}> Registruj se.</Text>
                           </Text>
                           
                           
                       </View>
                       <TouchableOpacity onPress={this.login} style={{ backgroundColor:"#1E272E", height:50,justifyContent:"center",borderRadius:15,alignItems:"center",marginTop:40}}>
                           
                           <Text style={{ fontWeight: "bold",fontSize:20, color:"#F9F9F9"}} 
                           >PRIJAVI SE</Text>
                       </TouchableOpacity>

                       
                   </View>

                </ImageBackground>
            </View>
        );
    }
}

const styles=StyleSheet.create({

    container: {
        flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#F5FCFF',
     flexDirection: 'column',

    },
    backgroundImage:{
        width:"100%",
        height:"100%",
      },
      prijava:{
          
          marginTop:"12%",
          marginLeft:"5%"

      },
      forma:{
        marginLeft:"5%",
        marginRight:"5%",
        marginTop:"20%"

      }
})