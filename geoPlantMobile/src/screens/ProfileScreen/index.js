import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon3 from 'react-native-vector-icons/Zocial';
import Icon4 from 'react-native-vector-icons/FontAwesome';
export default class ProfileScreen extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            ime:"",
            lozinka:"",
        }
    }
    backBtn=()=>{
        console.log("back");
    }
    editBtn=()=>{
        console.log("edit");
    }
    render()
    {
        return(
            <View style={styles.container}>
               
                   <View style={styles.gornjePola}>
                       
                        <View style={{width:"100%",
                        height:"15%",
                        opacity:0.3,
                        backgroundColor:"lightgrey",
                        justifyContent:"center"}}>
                            <Icon style={{marginLeft:"3%"}} 
                            size={50} 
                            color={"#656464"} 
                            name={"md-arrow-back"} 
                            onPress={this.backBtn} ></Icon>
                        </View>
                   </View>
                   <View style={styles.kruzic}>
                   <Icon2  size={35}
                    color={"white"} 
                    name={"edit"} 
                    onPress={this.edit} ></Icon2>
                   </View>
                   <View style={styles.donjePola}>
                       <View style={{marginTop:"10%",
                       alignItems:"center",
                       backgroundColor:"#1E272E",
                       heigth:"10%"}}>
                           <Text style={{color:"white",fontSize:33,fontWeight:"bold"}}>EMILY BERG</Text>
                           <Text style={{color:"white",fontSize:20}}>Oregon,USA</Text>

                       </View>
                       <View style={{flexDirection:"row"}}>
                       <View style={{marginLeft:"10%",marginTop:"5%",width:"50%",justifyContent:"center",alignItems:"flex-start"}}>
                            <Text style={{color:"#32664D",fontSize:17}}>Godine:</Text>
                            
                            <Text style={{color:"white",fontSize:20,marginTop:"3%"}}>28 godina </Text>
                    
                       
                    
                       </View>
                       <View style={{marginLeft:"10%",marginTop:"5%",width:"50%",justifyContent:"center",alignItems:"flex-start"}}>
                            <Text style={{color:"#32664D",fontSize:17}}>Zvanje:</Text>
                            
                            <Text style={{color:"white",fontSize:20,marginTop:"3%"}}>Neko..</Text>
                    
                       
                    
                       </View>
                       </View>
                       <View style={{marginTop:"10%",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
                       <Icon3   size={20}
                                color={"#656464"}
                                name={"email"}> 
                                
                       </Icon3>
                       <Text style={{fontSize:17,color:"white"}}>  emilyberg@gmail.com  </Text>    
                       </View>
                       <View style={{marginTop:"3%",justifyContent:"center",alignItems:"center",flexDirection:"row"}}>
                       <Icon4   size={20}
                                color={"#656464"}
                                name={"phone"}> 
                                
                       </Icon4>
                       <Text style={{fontSize:17,color:"white"}}>  +1 312-888-3041  </Text>    
                       </View>
                      
                      
                       
                        
                   </View>
                   
                  

                
            </View>
        );
    }
}

const styles=StyleSheet.create({

    container: {
    flex:1,
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: 'blue',
     flexDirection: 'column',
     fontFamily:"Garamond"

    },
    gornjePola: {
        width:"100%",
        height:"50%",
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'column',
   
       },
       donjePola: {
        width:"100%",
        height:"50%",
        backgroundColor:"#1E272E"
   
       },
       kruzic:{
           borderRadius:40,
           width:58,
           height:58,
           backgroundColor:"#32664D",
           position:"absolute",
           left:"10%",
           justifyContent:"center",
           zIndex:9999,
           justifyContent:"center",
           alignItems:"center"
           


       },
    backgroundImage:{
        width:"100%",
        height:"100%",
      },
      prijava:{
          
          marginTop:"12%",
          marginLeft:"5%"

      },
      forma:{
        marginLeft:"5%",
        marginRight:"5%",
        marginTop:"20%"

      }
})