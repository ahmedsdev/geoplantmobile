import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,Image,TextInput,TouchableOpacity} from 'react-native';


export default class Biljka extends Component{
 
    render()
    {
        return(
            <View style={styles.container}>
                <View style={styles.polaView}>
                  <Text style={styles.textInput} placeholder={"Attor"} placeholderTextColor={"white"}>Autor</Text>
                  <Text style={styles.textInput} placeholder={"Ime"} placeholderTextColor={"white"}>{this.props.biljka.name}</Text>
                  <Text style={styles.textInput} placeholder={"Lokacija"} placeholderTextColor={"white"}>Lokacija</Text>
                  <TouchableOpacity
                  style={styles.btn}
                  >
                      <Text style={{color:'white',fontSize:14}}>Opširnije...</Text>
                  </TouchableOpacity>
                </View>
                <Image source={{uri: this.props.biljka.image}} style={{width:"45%",justifyContent:"center",height:"100%"}}>
                  
                </Image>
            </View>
        );
    }
}

const styles=StyleSheet.create({

    container: {
        width:'100%',
        height:'100%',
     justifyContent: 'center',
     alignItems: 'center',
     flexDirection: 'column',
     borderRadius:25,
     flexDirection:'row'
    },
    backgroundImage:{
        width:"100%",
        height:"100%",


      },
      polaView:{
          width:'55%',
          height:'100%',
          alignItems:'center',
          justifyContent:'center'
      },
      textInput:{
          borderColor:'gray',
          borderBottomWidth:2,
          width:'90%',
          height:'24%',
          padding:0,
          margin:0,
          paddingLeft:5,
          marginBottom:'1%',
          backgroundColor:'transparent',
          color:'white'
      },
      btn:{
          backgroundColor:'#316049',
          width:'60%',
          height:'20%',
          alignItems:'center',
          justifyContent:'center',
          borderRadius:7,
          marginTop:'5%'
      }
      
})