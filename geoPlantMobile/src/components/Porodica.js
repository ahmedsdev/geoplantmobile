import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,Image,TouchableOpacity} from 'react-native';
import {withNavigation} from 'react-navigation';

class Porodica extends Component{
 
    clanoviPorodice=() => {
        this.props.navigation.navigate('PrikazBiljakaScreen',{id:this.props.porodica.id});
    }

    render()
    {
        return(
            <TouchableOpacity onPress={this.clanoviPorodice}>

                <View style={styles.container}>
                    <Image source={{uri:this.props.porodica.image}} style={{borderRadius:25,width:"100%",justifyContent:"center",height:"100%"}}>
                    
                    </Image>
                    <View style={{position:'absolute',backgroundColor:"gray",height:"20%",width:"100%",alignItems:"center",opacity:0.7,justifyContent:"center"}}>

                        <Text style={{color:"white",textAlign:"center"}}>
                            {this.props.porodica.name}
                        </Text>
                    </View>
                </View>              
            </TouchableOpacity>
        );
    }
}

const styles=StyleSheet.create({

    container: {
        width:'100%',
        height:'100%',
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#F5FCFF',
     flexDirection: 'column',
     borderRadius:25
    },
    backgroundImage:{
        width:"100%",
        height:"100%",


      },
      
})
export default withNavigation(Porodica);