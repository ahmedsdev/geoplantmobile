import React,{Component} from 'react';
import {Text,View,StyleSheet,ImageBackground,Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {withNavigation} from 'react-navigation';

 class Header extends Component{
 
constructor(props){
    super(props);
    this.state={

    };
}

    render()
    {
        return(
               <View style={styles.UrediProfil}>
                       <View style={{height:"100%",width:"15%",justifyContent:"center",alignItems:"center"}}>
                       <Icon
                       onPress = {() => {this.props.navigation.goBack()}}
                       name="ios-arrow-back"
                       size={40}
                       color="white"
                       ></Icon>
                       </View>
                       <View style={{height:"100%",width:"70%",justifyContent:"center",alignItems:"center"}}>
                       <Text style={{fontSize:25, 
                        color:"white"}} >{this.props.naslov}</Text>
                       </View>
                     </View>
        );
    }
}

const styles=StyleSheet.create({

    container: {
        width:'100%',
        height:'100%',
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#F5FCFF',
     flexDirection: 'column',
     borderRadius:25
    },

    
    UrediProfil:{
         
        height:70,
        width:"100%",
        backgroundColor:"#31664C",
        flexDirection:"row",
       

    },
   
})

export default withNavigation(Header);