import {createStackNavigator,createAppContainer, createBottomTabNavigator,createDrawerNavigator, createSwitchNavigator} from 'react-navigation';

import LoginScreen from './src/screens/LoginScreen';
import RegistrationScreen from './src/screens/RegistrationScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import AddScreen from './src/screens/AddScreen';
import EditProfileScreen from './src/screens/EditProfileScreen';
import SplashScreen from './src/screens/SplashScreen';
import SettingsScreen from './src/screens/SettingsScreen';
import MainScreen from './src/screens/MainScreen';
import HerbarScreen from './src/screens/HerbarScreen';
import PorodiceScreen from './src/screens/PorodiceScreen';
import PrikazBiljakaScreen from './src/screens/PrikazBiljakaScreen';
import SettingsScreen2 from './src/screens/SettingsScreen2';
import React from 'react';

const Auth = createStackNavigator({
  LoginScreen: {screen: LoginScreen},
  RegistrationScreen:{screen:RegistrationScreen},
  SplashScreen:{screen:SplashScreen}
},
{
  headerMode: 'none',
  initialRouteName:'LoginScreen',
  navigationOptions: {
      headerVisible: false,
  }
});

const MainNavigator = createStackNavigator({
  ProfileScreen: ProfileScreen,
  AddScreen: AddScreen, 
  EditProfileScreen: EditProfileScreen,
  SettingsScreen: SettingsScreen,
  MainScreen:MainScreen,
  HerbarScreen:HerbarScreen,
  PorodiceScreen:PorodiceScreen,
  PrikazBiljakaScreen:PrikazBiljakaScreen,
  SettingsScreen2: SettingsScreen2,
},
{
  headerMode: 'none',
  initialRouteName:'MainScreen',
  navigationOptions: {
      headerVisible: false,
  }
});
const Drawer=createDrawerNavigator({
  ProfileStack:{
    screen:Auth,
     navigationOptions: {
      drawerLabel: () => null
 }
  },
  HomeStack:MainNavigator,
  Herbar:HerbarScreen,
  RegistrationScreen,
  AddScreen,
  EditProfileScreen,
  SettingsScreen2,
  PrikazBiljakaScreen,
  PorodiceScreen,
  SplashScreen
}
)
const Switch=createSwitchNavigator({
  Auth,
  MainNavigator,
  Drawer
},{headerMode:'none'})
export default createAppContainer(Drawer);